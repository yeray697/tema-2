###### La aplicación tiene unos estilos de botones y EditText personalizado.
###### También tiene un icono personalizado
# 1. Crear una aplicación que permita añadir a una agenda los nombres, teléfonos y emails de nuestros amigos. Los datos se guardarán en un fichero en memoria interna.
###### Muestra una lista y un floating action button.
###### Al pulsar sobre un elemento de la lista, abre otra activity para ver los datos, modificarlo o eliminarlo (pide confirmación).
###### El campo del nombre y el teléfono son obligatorios, el del email no.
###### Controla que no se repita el número de teléfono, impidiendo que se añada el contacto

# 2. Se desea programar varias alarmas (cinco como máximo) para que suenen de forma consecutiva cada cierto intervalo de tiempo.
###### Muestra una lista, un botón para comenzar el cronómetro y un floating action button.
###### Al pulsar sobre un elemento de la lista, abre otra activity para ver los datos, modificarlo o eliminarlo (pide confirmación).
###### Las alarmas admiten minutos decimales
###### Se controla que sólo puedan haber 5 alarmas como máximo.

# 3. Crear una aplicación para calcular los días fértiles de una mujer.
###### Para obtener la fecha dividimos entre dos la duración media del ciclo menstrual, le restamos dos, y se lo sumamos a la fecha de la regla y ese día y los 4 siguientes serán los más fértiles.
###### En caso de encontrarse en uno de esos días, mostrará un mensaje, tanto al cargar el fichero como al introducir una fecha nueva.

# 4. Crear una aplicación que pida una dirección web y muestre en pantalla la página indicada en esa dirección. Se podrá elegir entre java.net, Android Asynchronous Http Client o Volley para realizar la conexión con el servidor web
###### Muestra el tiempo que tarda en cargar cada página

# 5. Crear una aplicación que permita ver una secuencia de imágenes descargadas de Internet.
###### Puedes elegir de dónde carga los txt, por defecto tiene una.
###### En caso de no haber internet muestra una imagen por defecto.
###### En caso de no existir la imagen muestra otra imagen por defecto.
###### Cachea las imágenes gracias a la librería Picasso.
###### Comprueba que el txt con las imágenes sean rutas válidas (que sean una dirección web vñálida y que acaben en .jpg y .png). En el caso de que no lo sean, no las añade al Array.
###### Tiene un contador para saber qué número de imagen es (Ejemplo: imagen 1 de 5).
###### Puedes recorrer las imágenes con dos botones, que recorren el array de forma circular.

# 6. Modificar la aplicación Conversor de moneda para que utilice el fichero cambio.txt (con el ratio del cambio euros/dólares) almacenado en un servidor web situado en otro equipo del aula o en Internet.
###### Carga el fichero de una web y lo guarda en el móvil
###### En caso de que no haya internet avisará, pero si tiene el fichero guardado, lo hará igualmente.
###### Tiene un pequeño fallo, y es que al ser un método asíncrono, por rapidez, primero convierte y luego descarga. Saldrá un mensaje la primera vez que se use avisándolo, y basta con darle al botón de nuevo.

# 7. Crear una aplicación que permita elegir un fichero usando algún explorador de archivos instalado en el dispositivo y lo suba a la carpeta uploads situada en el servidor web del propio equipo o en Internet.
###### Avisa en que url se está subiendo. (Para cambiarla hay que hacerlo desde Android Studio)
