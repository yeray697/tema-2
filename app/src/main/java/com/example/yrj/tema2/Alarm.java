package com.example.yrj.tema2;

/**
 * Created by yeray697 on 2/11/16.
 */

public class Alarm {
    static final char SEPARATOR = ';';

    private String time;
    private String name;

    public Alarm(String time, String name){
        this.setName(name);
        this.time = time;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (name.contains(";"))
            name.replace(';',' ');
        this.name = name;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String toFile(){
        String file = "";
        file = this.time + this.SEPARATOR + this.name + "\n";
        return file;
    }

    @Override
    public String toString() {
        String devolver = "";
        if (this.time.equals("1"))
            devolver = "1 minuto";
        else
            devolver = this.time +" minutos";
        return devolver;
    }
}
