package com.example.yrj.tema2;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Alarms_Activity extends AppCompatActivity {

    EditText etName, etTime;
    Button btAdd, btEdit, btRemove;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarms);
        setTitle("Prueba");
        etName = (EditText) findViewById(R.id.etName2);
        etTime = (EditText) findViewById(R.id.etTime2);
        btAdd = (Button) findViewById(R.id.btAdd2);
        btEdit = (Button) findViewById(R.id.btEdit2);
        btRemove = (Button) findViewById(R.id.btRemove2);
        //Obtenemos qué es lo que va a hacer la activity
        int action = getIntent().getExtras().getInt("action");
        switch (action){
            case Exercise1_Activity.ADDCONTACT_CODE:
                btAdd.setVisibility(View.VISIBLE);
                setTitle("Añadir una alarma");
                break;
            case Exercise1_Activity.SHOWCONTACT_CODE:
                setTitle("Alarma");
                btEdit.setVisibility(View.VISIBLE);
                btRemove.setVisibility(View.VISIBLE);
                etName.setText(getIntent().getExtras().getString("name"));
                etTime.setText(getIntent().getExtras().getString("time"));
                break;
        }
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name, time;
                name = etName.getText().toString();
                time = etTime.getText().toString();
                if (name.length() > 0) {
                    if (time.length() > 0) {
                        try {
                            time = time.replace(',','.');
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("name",name);
                            returnIntent.putExtra("time", String.valueOf(Double.parseDouble(time)));
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();
                        } catch (Exception e) {
                            Toast.makeText(Alarms_Activity.this, "No has introducido un tiempo válido", Toast.LENGTH_SHORT).show();
                        }
                    } else
                        Toast.makeText(Alarms_Activity.this, "El campo tiempo no puede estar vacío", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(Alarms_Activity.this, "El campo nombre no puede estar vacío", Toast.LENGTH_SHORT).show();
            }
        });
        btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Alarms_Activity.this);
                alertDialogBuilder.setTitle("Eliminar alarma")
                        .setMessage("¿Desea eliminar la alarma? No podrá recuperarlo")
                        .setCancelable(false)
                        .setPositiveButton("Sí",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("eliminar", true);

                                returnIntent.putExtra("name", getIntent().getExtras().getString("name"));
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name, time;
                name = etName.getText().toString();
                time = etTime.getText().toString();
                if (name.length() > 0) {
                    if (time.length() > 0) {
                        try {
                            time = time.replace(',','.');
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("modificar",true);

                            returnIntent.putExtra("newname",name);
                            returnIntent.putExtra("newtime",String.valueOf(Double.parseDouble(time)));

                            returnIntent.putExtra("oldname", getIntent().getExtras().getString("name"));
                            returnIntent.putExtra("oldtime", getIntent().getExtras().getString("time"));
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();
                        } catch (Exception e) {
                        Toast.makeText(Alarms_Activity.this, "No has introducido un tiempo válido", Toast.LENGTH_SHORT).show();
                    }
                    } else
                        Toast.makeText(Alarms_Activity.this, "El campo tiempo no puede estar vacío", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(Alarms_Activity.this, "El campo nombre no puede estar vacío", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
