package com.example.yrj.tema2;

/**
 * Created by usuario on 24/10/16.
 */

public class Contact {
    static final char SEPARATOR = ';';

    private String name;
    private String phone;
    private String email;

    public Contact(String name, String phone, String email){
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public Contact(String name, String phone){
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toFile(){
        String file = "";
        if (this.email == null){
            file = this.name + this.SEPARATOR + this.phone + "\n";
        } else {
            file = this.name + this.SEPARATOR + this.phone + this.SEPARATOR + this.email + "\n";
        }
        return file;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
