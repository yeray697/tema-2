package com.example.yrj.tema2;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Contacts_Activity extends AppCompatActivity {

    EditText etName, etPhone, etEmail;
    Button btAdd, btEdit, btRemove;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        etName = (EditText) findViewById(R.id.etName1);
        etPhone = (EditText) findViewById(R.id.etPhone1);
        etEmail = (EditText) findViewById(R.id.etEmail1);
        btAdd = (Button) findViewById(R.id.btAdd1);
        btEdit = (Button) findViewById(R.id.btEdit1);
        btRemove = (Button) findViewById(R.id.btRemove1);
        //Obtenemos qué es lo que va a hacer la activity
        int action = getIntent().getExtras().getInt("action");
        switch (action){
            case Exercise1_Activity.ADDCONTACT_CODE:
                btAdd.setVisibility(View.VISIBLE);
                setTitle("Añadir un contacto");
                break;
            case Exercise1_Activity.SHOWCONTACT_CODE:
                setTitle("Contacto");
                btEdit.setVisibility(View.VISIBLE);
                btRemove.setVisibility(View.VISIBLE);
                etName.setText(getIntent().getExtras().getString("name"));
                etPhone.setText(getIntent().getExtras().getString("phone"));
                etEmail.setText(getIntent().getExtras().getString("email"));
                break;
        }
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name, phone, email;
                name = etName.getText().toString();
                phone = etPhone.getText().toString();
                email = etEmail.getText().toString();
                if (name.length() > 0) {
                    if (phone.length() > 0) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("name",name);
                        returnIntent.putExtra("phone",phone);
                        //El email es opcional
                        returnIntent.putExtra("email",email);
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                    } else
                        Toast.makeText(Contacts_Activity.this, "El campo teléfono no puede estar vacío", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(Contacts_Activity.this, "El campo nombre no puede estar vacío", Toast.LENGTH_SHORT).show();
            }
        });
        btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Contacts_Activity.this);
                alertDialogBuilder.setTitle("Eliminar contacto")
                        .setMessage("¿Desea eliminar el contacto? No podrá recuperarlo")
                        .setCancelable(false)
                        .setPositiveButton("Sí",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("eliminar", true);
                                //Cogemos el teléfono inicial
                                returnIntent.putExtra("phone", getIntent().getExtras().getString("phone"));
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }
                        })
                        .setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name, phone, email;
                name = etName.getText().toString();
                phone = etPhone.getText().toString();
                email = etEmail.getText().toString();
                if (name.length() > 0) {
                    if (phone.length() > 0) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("modificar",true);
                        returnIntent.putExtra("newname",name);
                        returnIntent.putExtra("newphone",phone);
                        //El email es opcional
                        returnIntent.putExtra("newemail",email);

                        returnIntent.putExtra("oldname", getIntent().getExtras().getString("name"));
                        returnIntent.putExtra("oldphone", getIntent().getExtras().getString("phone"));
                        returnIntent.putExtra("oldemail", getIntent().getExtras().getString("email"));
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                    } else
                        Toast.makeText(Contacts_Activity.this, "El campo teléfono no puede estar vacío", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(Contacts_Activity.this, "El campo nombre no puede estar vacío", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
