package com.example.yrj.tema2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Exercise1_Activity extends AppCompatActivity{

    public static final int ADDCONTACT_CODE = 1;
    public static final int SHOWCONTACT_CODE = 2;
    String internalFile = "contacts.txt";
    final String CODIFICATION = "UTF-8";
    ArrayList<Contact> contacts;
    ArrayAdapter<Contact> adapter;
    FloatingActionButton fabAdd;
    ListView lvContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise1);
        //Setting title
        setTitle(getResources().getString(R.string.exercise1Title));
        //Setting controls
        fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd1);
        lvContacts = (ListView) findViewById(R.id.lvContacts1);
        //Events
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Exercise1_Activity.this,Contacts_Activity.class);
                intent.putExtra("action",ADDCONTACT_CODE);
                startActivityForResult(intent,ADDCONTACT_CODE);
            }
        });
        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Exercise1_Activity.this,Contacts_Activity.class);
                Contact aux = contacts.get(i);
                intent.putExtra("name", aux.getName());
                intent.putExtra("email", aux.getEmail());
                intent.putExtra("phone", aux.getPhone());
                intent.putExtra("action",SHOWCONTACT_CODE);
                startActivityForResult(intent,SHOWCONTACT_CODE);
            }
        });
        //Getting previous contacts
        contacts = new ArrayList<Contact>();
        adapter = new ArrayAdapter<Contact>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, contacts);
        lvContacts.setAdapter(adapter);
        getContacts();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADDCONTACT_CODE){
            if (resultCode == RESULT_OK){
                String name = data.getExtras().getString("name");
                String email = data.getExtras().getString("email");
                String phone = data.getExtras().getString("phone");
                Contact aux;
                if (email == "")
                    aux = new Contact(name,phone);
                else
                    aux = new Contact(name,phone, email);
                addContact(aux);
            }
        } else if (requestCode == SHOWCONTACT_CODE){

            if (resultCode == RESULT_OK){
                if (data.getExtras().getBoolean("eliminar",false)) { //Si entra aquí, se pulsó el botón eliminar
                    removeContact(data.getExtras().getString("phone"));
                } else { //Si no, se pulsó el botón modificar
                    Contact oldContact, newContact;
                    String oldName, oldPhone, oldEmail;
                    String newName, newPhone, newEmail;
                    oldName = data.getExtras().getString("oldname");
                    oldPhone = data.getExtras().getString("oldphone");
                    oldEmail = data.getExtras().getString("oldemail");
                    if (oldEmail == "")
                        oldContact = new Contact(oldName,oldPhone);
                    else
                        oldContact = new Contact(oldName,oldPhone,oldEmail);

                    newName = data.getExtras().getString("newname");
                    newPhone = data.getExtras().getString("newphone");
                    newEmail = data.getExtras().getString("newemail");
                    if (newEmail == "")
                        newContact = new Contact(newName,newPhone);
                    else
                        newContact = new Contact(newName,newPhone,newEmail);
                    modifyContact(oldContact, newContact);
                }
            }
        }
    }

    private void updateList() {
        adapter.notifyDataSetChanged();
    }

    private void getContacts() {
        String file = Utilities.readInternal(this.getFilesDir().getAbsolutePath(),internalFile,CODIFICATION);
        if (file == "") {//if file does not exist, we create it by default
            Utilities.writeInternal(getFilesDir().getAbsolutePath(),internalFile,file,true,CODIFICATION);
        } else {
            parseFile(file);
            updateList();
        }
    }

    private void parseFile(String file){
        Contact contactAux;
        String name = "";
        String phone = "";
        String email = "";
        String[] dataAux;
        for (String lane: file.split("\n")) {
            dataAux = lane.split(String.valueOf(Contact.SEPARATOR));
            if (dataAux.length == 2) { //It does not contain the email
                name = dataAux[0];
                phone = dataAux[1];
                contactAux = new Contact(name, phone);
            } else {
                name = dataAux[0];
                phone = dataAux[1];
                email = dataAux[2];
                contactAux = new Contact(name, phone, email);
            }
            contacts.add(contactAux);
        }
    }

    private void addContact(Contact contact){
        boolean repetido = false;
        //Recorremos los contactos para comprobar que no están repetidos
        for (Contact auxContact:contacts) {
            if (auxContact.getPhone().equals(contact.getPhone())){
                repetido = true;
                break;
            }
        }
        if (repetido)
            Toast.makeText(this, "El número de teléfono está repetido", Toast.LENGTH_SHORT).show();
        else {
            String contactFile = contact.toFile();
            Utilities.writeInternal(getFilesDir().getAbsolutePath(),internalFile,contactFile,true,CODIFICATION);
            contacts.add(contact);
            updateList();
        }
    }

    private void removeContact(String phone){
        String textoAGuardar = "";
        boolean eliminado = false;
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).getPhone().equals(phone)){
                contacts.remove(contacts.get(i));
                Toast.makeText(this, "Eliminado correctamente", Toast.LENGTH_SHORT).show();
                eliminado = true;
                i--;
            } else
                textoAGuardar = contacts.get(i).toFile();
        }
        if (eliminado) {
            Utilities.writeInternal(getFilesDir().getAbsolutePath(),internalFile,textoAGuardar,false,CODIFICATION);
            updateList();
        }
    }

    private void modifyContact(Contact oldContact, Contact newContact){
        String textoAGuardar = "";
        boolean modificado = false;
        for (Contact auxContact:contacts) {
            if (auxContact.getPhone().equals(oldContact.getPhone())){
                auxContact.setName(newContact.getName());
                auxContact.setEmail(newContact.getEmail());
                auxContact.setPhone(newContact.getPhone());
                modificado = true;
            }
            textoAGuardar = auxContact.toFile();
        }if (modificado) {
            Utilities.writeInternal(getFilesDir().getAbsolutePath(),internalFile,textoAGuardar,false,CODIFICATION);
            updateList();
            Toast.makeText(this, "Modificado correctamente", Toast.LENGTH_SHORT).show();
        }
    }
}
