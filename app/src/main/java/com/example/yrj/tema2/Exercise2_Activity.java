package com.example.yrj.tema2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Exercise2_Activity extends AppCompatActivity {

    public static final int ADDCONTACT_CODE = 1;
    public static final int SHOWCONTACT_CODE = 2;
    String externalFile = "alarm.txt";
    final String CODIFICATION = "UTF-8";
    ArrayList<Alarm> alarms;
    ArrayAdapter<Alarm> adapter;
    FloatingActionButton fabAdd;
    ListView lvContacts;
    Button btStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise2);
        //Setting title
        setTitle(getResources().getString(R.string.exercise2Title));
        //Setting controls
        fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd2);
        lvContacts = (ListView) findViewById(R.id.lvAlarms2);
        btStart = (Button) findViewById(R.id.btStart2);
        //Events
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Exercise2_Activity.this,Alarms_Activity.class);
                intent.putExtra("action",ADDCONTACT_CODE);
                startActivityForResult(intent,ADDCONTACT_CODE);
            }
        });
        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Exercise2_Activity.this,Alarms_Activity.class);
                Alarm aux = alarms.get(i);
                intent.putExtra("name", aux.getName());
                intent.putExtra("time", aux.getTime());
                intent.putExtra("action",SHOWCONTACT_CODE);
                startActivityForResult(intent,SHOWCONTACT_CODE);
            }
        });
        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alarms.size() > 0) {
                    Intent intent = new Intent(Exercise2_Activity.this, StartAlarms_Activity.class);
                    for (int i = 0; i < alarms.size(); i++) {
                        intent.putExtra("time" + i, alarms.get(i).getTime());
                        intent.putExtra("name" + i, alarms.get(i).getName());
                    }
                    startActivity(intent);
                }
            }
        });
        //Getting previous contacts
        alarms = new ArrayList<Alarm>();
        adapter = new ArrayAdapter<Alarm>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, alarms);
        lvContacts.setAdapter(adapter);
        getContacts();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADDCONTACT_CODE){
            if (resultCode == RESULT_OK){
                String name = data.getExtras().getString("name");
                String time = data.getExtras().getString("time");
                Alarm aux = new Alarm(time, name);
                addAlarm(aux);
            }
        } else if (requestCode == SHOWCONTACT_CODE){

            if (resultCode == RESULT_OK){
                if (data.getExtras().getBoolean("eliminar",false)) { //Si entra aquí, se pulsó el botón eliminar
                    removeAlarm(data.getExtras().getString("name"));
                    checkLimit();
                } else { //Si no, se pulsó el botón modificar
                    Alarm oldAlarm, newAlarm;
                    String oldName, oldTime;
                    String newName, newTime;
                    oldName = data.getExtras().getString("oldname");
                    oldTime = data.getExtras().getString("oldtime");
                    oldAlarm = new Alarm(oldTime,oldName);

                    newName = data.getExtras().getString("newname");
                    newTime = data.getExtras().getString("newtime");
                    newAlarm = new Alarm(newTime,newName);
                    modifyAlarm(oldAlarm, newAlarm);
                }
            }
        }
    }

    private void updateList() {
        adapter.notifyDataSetChanged();
    }

    private void getContacts() {
        String file = Utilities.readExternal(externalFile,CODIFICATION);
        if (file == "") {//if file does not exist, we create it by default
            Utilities.writeInternal(getFilesDir().getAbsolutePath(), externalFile,file,true,CODIFICATION);
        } else {
            parseFile(file);
            updateList();
            checkLimit();
        }
    }

    private void parseFile(String file){
        Alarm alarmaAux;
        String name = "";
        String time = "";
        String[] dataAux;
        for (String lane: file.split("\n")) {
            dataAux = lane.split(String.valueOf(Contact.SEPARATOR));
            time = dataAux[0];
            name = dataAux[1];
            alarmaAux = new Alarm(time, name);
            alarms.add(alarmaAux);
        }
    }

    private void addAlarm(Alarm alarm){
        String contactFile = alarm.toFile();
        Utilities.writeExternal(externalFile,contactFile,true,CODIFICATION);
        alarms.add(alarm);
        updateList();
        checkLimit();
    }

    private void checkLimit() {
        if (alarms.size() == 5)
            fabAdd.setVisibility(View.GONE);
        else
            fabAdd.setVisibility(View.VISIBLE);
    }

    private void removeAlarm(String name){
        String textoAGuardar = "";
        boolean eliminado = false;
        for (int i = 0; i < alarms.size(); i++) {
            if (alarms.get(i).getName().equals(name)){
                alarms.remove(alarms.get(i));
                Toast.makeText(this, "Eliminado correctamente", Toast.LENGTH_SHORT).show();
                eliminado = true;
                i--;
            } else
                textoAGuardar = alarms.get(i).toFile();
        }
        if (eliminado) {
            Utilities.writeInternal(getFilesDir().getAbsolutePath(), externalFile,textoAGuardar,false,CODIFICATION);
            updateList();
        }
    }

    private void modifyAlarm(Alarm oldAlarm, Alarm newAlarm){
        String textoAGuardar = "";
        boolean modificado = false;
        for (Alarm auxAlarm:alarms) {
            if (auxAlarm.getName().equals(oldAlarm.getName())){
                auxAlarm.setName(newAlarm.getName());
                auxAlarm.setTime(newAlarm.getTime());
                modificado = true;
            }
            textoAGuardar = auxAlarm.toFile();
        }if (modificado) {
            Utilities.writeExternal(externalFile,textoAGuardar,false,CODIFICATION);
            updateList();
            Toast.makeText(this, "Modificado correctamente", Toast.LENGTH_SHORT).show();
        }
    }
}