package com.example.yrj.tema2;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Exercise3_Activity extends AppCompatActivity {
    EditText etDuracion;
    DatePicker dpRegla;
    Button btSubmit;
    TextView tvResult;

    int day, month, year;
    SimpleDateFormat formatDate =  new SimpleDateFormat("dd/MM/yyyy");
    String filename = "fertil.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise3);
        //Setting title
        setTitle(getResources().getString(R.string.exercise3Title));

        etDuracion = (EditText) findViewById(R.id.etDuracion3);
        btSubmit = (Button) findViewById(R.id.btSubmit3);
        dpRegla = (DatePicker) findViewById(R.id.dpRegla3);
        tvResult = (TextView) findViewById(R.id.tvResult3);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                day = dpRegla.getDayOfMonth();
                month = dpRegla.getMonth();
                year = dpRegla.getYear();
                if (etDuracion.getText().toString().length()>0){
                    Date[] diasFertiles = calculate(Integer.parseInt(etDuracion.getText().toString()));
                    show(diasFertiles);
                    save(diasFertiles);
                } else {
                    Toast.makeText(Exercise3_Activity.this, "No has seleccionado la duración media", Toast.LENGTH_SHORT).show();
                }
            }
        });

        String[] diasFertilesString = Utilities.readExternal(filename,"UTF-8").split("\n");
        Date[] diasFertilesDate = new Date[5];
        try {
            diasFertilesDate[0] = formatDate.parse(diasFertilesString[0]);
            diasFertilesDate[1] = formatDate.parse(diasFertilesString[1]);
            diasFertilesDate[2] = formatDate.parse(diasFertilesString[2]);
            diasFertilesDate[3] = formatDate.parse(diasFertilesString[3]);
            diasFertilesDate[4] = formatDate.parse(diasFertilesString[4]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(diasFertilesDate[4] != null) {
            show(diasFertilesDate);

        }
    }
    private void show(Date[] diasFertiles) {
        String aux = "Los días más fértiles son:\n";
        for (int i = 0; i < 5; i++)
            aux +=  formatDate.format(diasFertiles[i])+"\n";
        tvResult.setText(aux);
        if (Calendar.getInstance().getTime().compareTo(diasFertiles[0]) >= 0
                &&
                Calendar.getInstance().getTime().compareTo(diasFertiles[4]) <= 0) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false)
                    .setTitle("CUIDADO/APROVECHA")
                    .setMessage("Estás en tús días más fértiles")
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();

        }
    }

    private void save(Date[] diasFertiles) {
        String text = "";
        for (int i = 0; i < 5; i++) {
            text += formatDate.format(diasFertiles[i]);
            if (i != 4)
                text += "\n";
        }
        Utilities.writeExternal(filename,text,false,"UTF-8");
    }

    private Date[] calculate(int cicloLength) {
        Date[] diasFertiles = new Date[5];
        int cicloAux = cicloLength / 2 - 2;

        final Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,day);
        calendar.add(Calendar.DATE, cicloAux);

        Date dateAux;
        for (int i = 0; i < 5; i++) {
            dateAux = calendar.getTime();
            diasFertiles[i] = dateAux;
            calendar.add(Calendar.DATE,1);
        }
        return diasFertiles;
    }
}
