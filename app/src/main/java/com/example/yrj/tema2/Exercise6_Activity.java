package com.example.yrj.tema2;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Exercise6_Activity extends AppCompatActivity {

    EditText etEuro, etDollar;
    RadioButton rbEuroToDollar, rbDollarToEuro;
    Button btConvert;

    String fileRoute = "http://yeray697.esy.es/images/onedollarisoneeuro.txt";
    String fileName = "cambio66655.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise6);
        //Setting title
        setTitle(getResources().getString(R.string.exercise6Title));
        etEuro = (EditText) findViewById(R.id.etEuro6);
        etDollar = (EditText) findViewById(R.id.etDollar6);
        rbEuroToDollar = (RadioButton) findViewById(R.id.rbEuroToDollar6);
        rbDollarToEuro = (RadioButton) findViewById(R.id.rbDollarToEuro6);
        btConvert = (Button) findViewById(R.id.btConvert6);

        btConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convert(etEuro.getText().toString(), etDollar.getText().toString(), rbEuroToDollar.isChecked());
            }
        });
    }

    private void convert(String euro, String dollar, boolean isEuroToDollar) {
        if (isNetworkAvailable()) {
            try {
                new Download().execute();

            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            Toast.makeText(this, "No se ha podido descargar la última versión de los ratios, no hay internet", Toast.LENGTH_SHORT).show();
        }

        String conversion = Utilities.readExternal(fileName, "UTF-8").replace("\n", "");
        if (conversion.length() > 0) {
            Toast.makeText(Exercise6_Activity.this, "Ratio actual:\n1 dólar = " + conversion + " euros", Toast.LENGTH_SHORT).show();
            double result;

            try {
                if (isEuroToDollar) {
                    result = Double.parseDouble(euro) / Double.parseDouble(conversion);
                    etDollar.setText(String.valueOf(result));
                } else {
                    result = Double.parseDouble(dollar) * Double.parseDouble(conversion);
                    etEuro.setText(String.valueOf(result));
                }
            } catch (Exception ex) {
                Toast.makeText(this, "No puedes dejar el campo vacío", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "El ratio está guardándose (hace falta darle de nuevo a \"Convertir\") o no hay conexión a internet para descargarlo por primera vez. Vuelve a intentarlo", Toast.LENGTH_LONG).show();
        }
    }
    
    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    
    class Download extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                URL url = new URL(fileRoute);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream is = url.openStream();
                File file = new File(Environment.getExternalStorageDirectory() + "/" + fileName);
                if (!file.exists())
                    file.createNewFile();

                FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory() + "/"+fileName);
                byte data[] = new byte[1024];
                long total = 0;
                int count = 0;
                while ((count = is.read(data)) != -1) {
                    total += count;
                    int progress_temp = (int) total * 100 / lenghtOfFile;
                    fos.write(data, 0, count);
                }
                Handler handler =  new Handler(Exercise6_Activity.this.getMainLooper());
                handler.post( new Runnable(){
                    public void run(){
                        Toast.makeText(Exercise6_Activity.this,"Ratios actualizados correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
                is.close();
                fos.close();
            } catch (final Exception e) {
                Handler handler =  new Handler(Exercise6_Activity.this.getMainLooper());
                handler.post( new Runnable(){
                    public void run(){
                        Toast.makeText(Exercise6_Activity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }
    }
}
