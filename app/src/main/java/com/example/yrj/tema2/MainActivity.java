package com.example.yrj.tema2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String TAG = "com.example.yrj.tema2";
    private Button btExercise1,btExercise2,btExercise3,btExercise4,
            btExercise5,btExercise6,btExercise7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Setting title
        setTitle(getResources().getString(R.string.app_name));
        //Setting controls
        btExercise1 = (Button) findViewById(R.id.btExercise1);
        btExercise2 = (Button) findViewById(R.id.btExercise2);
        btExercise3 = (Button) findViewById(R.id.btExercise3);
        btExercise4 = (Button) findViewById(R.id.btExercise4);
        btExercise5 = (Button) findViewById(R.id.btExercise5);
        btExercise6 = (Button) findViewById(R.id.btExercise6);
        btExercise7 = (Button) findViewById(R.id.btExercise7);
        //Events
        btExercise1.setOnClickListener(this);
        btExercise2.setOnClickListener(this);
        btExercise3.setOnClickListener(this);
        btExercise4.setOnClickListener(this);
        btExercise5.setOnClickListener(this);
        btExercise6.setOnClickListener(this);
        btExercise7.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.btExercise1:
                intent = new Intent(MainActivity.this, Exercise1_Activity.class);
                break;
            case R.id.btExercise2:
                intent = new Intent(MainActivity.this, Exercise2_Activity.class);
                break;
            case R.id.btExercise3:
                intent = new Intent(MainActivity.this, Exercise3_Activity.class);
                break;
            case R.id.btExercise4:
                intent = new Intent(MainActivity.this, Exercise4_Activity.class);
                break;
            case R.id.btExercise5:
                intent = new Intent(MainActivity.this, Exercise5_Activity.class);
                break;
            case R.id.btExercise6:
                intent = new Intent(MainActivity.this, Exercise6_Activity.class);
                break;
            case R.id.btExercise7:
                intent = new Intent(MainActivity.this, Exercise7_Activity.class);
                break;
        }
        if (intent != null){
            startActivity(intent);
        }
    }
}
