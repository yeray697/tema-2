package com.example.yrj.tema2;

import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

public class StartAlarms_Activity extends AppCompatActivity {

    ArrayList<MyCountDownTimer> alarms;

    private static final long ONESECOND = 999;
    private static final long TIMEDEFAULT = 300050; //5 minutes
    private static final long INTERVAL = 5000; //5 seconds
    private TextView tvTime;
    private Button btStart;
    private Switch swOrder;
    private boolean timeOn; //If timeOn is true, buttons are disabled
    private boolean order; //If order is true, timer will be ascendant
    private int alarm = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_alarms);
        alarms = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        int contador = 0;
        while(!bundle.getString("name"+contador,"").equals("")){
            int time = (int)(Float.parseFloat(bundle.getString("time"+contador))*60000);
            String dialog = bundle.getString("name"+contador,"");
            alarms.add(new MyCountDownTimer(time, ONESECOND,dialog));
            contador++;
        }

        btStart = (Button) findViewById(R.id.btStart2);
        tvTime = (TextView) findViewById(R.id.tvTime2);
        swOrder = (Switch) findViewById(R.id.swOrder2);

        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (timeOn)
                    cancel();
                else
                    start();
            }
        });

        swOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                order = b;
            }
        });
        //Time
        timeOn = false;
        order = false;
    }

    /**
     * Cancel the chrono
     */
    private void cancel() {
        alarms.get(alarm).cancel();
        btStart.setText("Comenzar");
        tvTime.setText("Cancelado");
        timeOn = false;
    }

    /**
     * Start the chrono
     */
    private void start() {
        alarms.get(alarm).start();
        btStart.setText("Cancelar");
        timeOn = true;
    }

    /**
     * Set time on tvTime
     * @param time Time to set
     */
    protected void changeTime(String time){
        tvTime.setText(time);
    }

    /**
     * Method called when timer finish
     */
    protected void timerFinished(String dialog){
        timeOn = false;
        AlertDialog.Builder popup = new AlertDialog.Builder(this);
        popup.setTitle("Fin de la alarma "+(alarm + 1));
        popup.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (alarms.size() != alarm + 1 ){
                    alarm ++;
                    start();
                } else {
                    alarm = 0;
                    tvTime.setText("Terminado!");
                    btStart.setText("Comenzar");
                }
            }
        });
        popup.setCancelable(false);
        popup.setMessage(dialog);
        popup.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (alarms.get(alarm)!= null)
            alarms.get(alarm).cancel();
    }

    /**
     * Convert milliseconds to a string with the format xx:xx
     * @param time Time to convert
     * @return Time converted
     */
    private String msToTime(int time) {
        int minutes, seconds;
        String minutesAux, secondsAux;
        //Getting minutes
        minutes = (time / 1000)/60;
        if (minutes == 0)
            minutesAux = "00";
        else if (minutes < 10)
            minutesAux = "0"+minutes;
        else
            minutesAux = String.valueOf(minutes);
        //Getting seconds
        seconds = (time / 1000)%60;
        if (seconds == 0)
            secondsAux= "00";
        else if (seconds < 10)
            secondsAux = "0"+seconds;
        else
            secondsAux = String.valueOf(seconds);
        return minutesAux+":"+secondsAux;
    }

    /**
     * My chrono
     */
    protected class MyCountDownTimer extends CountDownTimer {

        String dialog;
        long time;
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public MyCountDownTimer(long millisInFuture, long countDownInterval,String dialog) {
            super(millisInFuture, countDownInterval);
            this.dialog = dialog;
            this.time = millisInFuture;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            String timeConverted;
            if(!order){ //Falling
                timeConverted = msToTime((int) millisUntilFinished);
            }
            else{ //Ascendant
                int timeAux = (int) (time - millisUntilFinished);
                timeConverted = msToTime(timeAux);
            }
            changeTime(timeConverted);
        }

        @Override
        public void onFinish() {
            timerFinished(this.dialog);
        }
    }

}