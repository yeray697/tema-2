package com.example.yrj.tema2;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utilities {
    private static boolean write(OutputStreamWriter file, String text){
        BufferedWriter bufferedWriter = null;
        Boolean result = false;
        try {
            bufferedWriter = new BufferedWriter(file);
            bufferedWriter.write(text);
            result = true;
        } catch (IOException e) {
            Log.e(MainActivity.TAG,"I/O Exception: "+e.getMessage());
        }
        finally {
            try {
                if (bufferedWriter != null)
                        bufferedWriter.close();
                else if (file != null)
                    file.close();
            } catch (IOException e) {
                Log.e(MainActivity.TAG,"I/O Exception while closing the file: "+e.getMessage());
            }
        }
        return result;
    }

    private static String read(File file, String codification){
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        StringBuilder result = new StringBuilder();
        int n;
        try {
            fileInputStream = new FileInputStream(file);
            inputStreamReader = new InputStreamReader(fileInputStream, codification);
            bufferedReader = new BufferedReader(inputStreamReader);
            while ((n = bufferedReader.read()) != -1)
                result.append((char) n);
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return result.toString();
    }

    public static boolean writeInternal(String route, String fileName, String text, Boolean appendText, String codification) {
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter;
        boolean result = false;
        File file = new File(route, fileName);
        try {
            fileOutputStream = new FileOutputStream(file, appendText);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream,codification);
            result = write(outputStreamWriter,text);
        }
        catch (Exception ex){
            Log.e(MainActivity.TAG,"I/O Exception writing in internal memory: "+ex.getMessage());
        }
        return result;

    }

    public static boolean writeExternal(String fileName, String text, Boolean appendText, String codification) {
        boolean result = false;
        if (canWriteExternal()) {
            OutputStreamWriter outputStreamWriter = null;
            FileOutputStream fileOutputStream = null;
            File file;
            try {
                file = new File(Environment.getExternalStorageDirectory(), fileName);
                fileOutputStream = new FileOutputStream(file, appendText);
                outputStreamWriter = new OutputStreamWriter(fileOutputStream,codification);
                result = write(outputStreamWriter,text);
            }
            catch (Exception ex){
                Log.e(MainActivity.TAG,"I/O Exception writing in external memory: "+ex.getMessage());
            }
        }
        return result;
    }

    public static String readInternal(String route, String fileName, String codification){
        File file;
        file = new File(route, fileName);
        return read(file, codification);
    }

    public static String readExternal(String fileName, String codification){
        String result = "";
        if (canReadExternal()){
            File file, externalCardName;
            externalCardName = Environment.getExternalStorageDirectory();
            file = new File(externalCardName.getAbsolutePath(), fileName);
            result = read(file, codification);
        }
        return result;
    }

    public static boolean canWriteExternal(){
        boolean canWrite = false;
//Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED))
            canWrite = true;
        return canWrite;
    }

    public static boolean canReadExternal(){
        boolean canRead = false;
//Comprobamos el estado de la memoria externa (tarjeta SD)
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)
                || state.equals(Environment.MEDIA_MOUNTED))
            canRead = true;
        return canRead;
    }

    public static String showInternalProperties (String route, String fileName) {
        File file;
        file = new File(route, fileName);
        return showProperties(file);
    }
    private static String showProperties (File file) {
        SimpleDateFormat dataFormat = null;
        StringBuffer txt = new StringBuffer();
        try {
            if (file.exists()) {
                txt.append("Nombre: " + file.getName() + '\n');
                txt.append("Ruta: " + file.getAbsolutePath() + '\n');
                txt.append("Tamaño (bytes): " + Long.toString(file.length()) + '\n');
                dataFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
                txt.append("Fecha: " + dataFormat.format(new Date(file.lastModified())) + '\n');
            } else
                txt.append("No existe el fichero " + file.getName() + '\n');
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            txt.append(e.getMessage());
        }
        return txt.toString();
    }
}